<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Users
/*Route::group(['prefix'=> 'users', 'middleware'=> 'auth'], function(){

    Route::get('/', ['as'=> 'user.index', 'uses'=> 'UserController@index'])->where('format', 'json|html');
    Route::get('/{user}', ['as'=> 'user.show', 'uses'=> 'UserController@show'])->where('user', '[0-9]+');

    Route::get('/create', ['as'=> 'user.create', 'uses'=> 'UserController@create']);
    Route::post('/', ['as'=> 'user.store', 'uses'=> 'UserController@store']);

    Route::get('/{user}/edit', ['as'=> 'user.edit', 'uses'=> 'UserController@edit']);
    Route::patch('/{user}', ['as'=> 'user.update', 'uses'=> 'UserController@update']);

    Route::get('/{user}/delete', ['as'=> 'user.destroy', 'uses'=> 'UserController@destroy']);
});*/

// Regions
Route::group(['prefix'=> 'regions', 'middleware'=> 'auth'], function(){

    Route::get('/', ['as'=> 'region.index', 'uses'=> 'RegionController@index'])->where('format', 'json|html');
    Route::get('/{region}', ['as'=> 'region.show', 'uses'=> 'RegionController@show'])->where('region', '[0-9]+');

    Route::get('/create', ['as'=> 'region.create', 'uses'=> 'RegionController@create']);
    Route::post('/', ['as'=> 'region.store', 'uses'=> 'RegionController@store']);

    Route::get('/{region}/edit', ['as'=> 'region.edit', 'uses'=> 'RegionController@edit']);
    Route::patch('/{region}', ['as'=> 'region.update', 'uses'=> 'RegionController@update']);

    Route::get('/{region}/delete', ['as'=> 'region.destroy', 'uses'=> 'RegionController@destroy']);
});

// Edificios
Route::group(['prefix'=> 'edificios', 'middleware'=> 'auth'], function(){

    Route::get('/', ['as'=> 'edificio.index', 'uses'=> 'EdificioController@index'])->where('format', 'json|html');
    Route::get('/{edificio}', ['as'=> 'edificio.show', 'uses'=> 'EdificioController@show'])->where('edificio', '[0-9]+');

    Route::get('/create', ['as'=> 'edificio.create', 'uses'=> 'EdificioController@create']);
    Route::post('/', ['as'=> 'edificio.store', 'uses'=> 'EdificioController@store']);

    Route::get('/{edificio}/edit', ['as'=> 'edificio.edit', 'uses'=> 'EdificioController@edit']);
    Route::patch('/{edificio}', ['as'=> 'edificio.update', 'uses'=> 'EdificioController@update']);

    Route::get('/{edificio}/delete', ['as'=> 'edificio.destroy', 'uses'=> 'EdificioController@destroy']);
});

// TipoSala
Route::group(['prefix'=> 'tiposalas', 'middleware'=> 'auth'], function(){

    Route::get('/', ['as'=> 'tiposala.index', 'uses'=> 'TipoSalaController@index'])->where('format', 'json|html');
    Route::get('/{tiposala}', ['as'=> 'tiposala.show', 'uses'=> 'TipoSalaController@show'])->where('tiposala', '[0-9]+');

    Route::get('/create', ['as'=> 'tiposala.create', 'uses'=> 'TipoSalaController@create']);
    Route::post('/', ['as'=> 'tiposala.store', 'uses'=> 'TipoSalaController@store']);

    Route::get('/{tiposala}/edit', ['as'=> 'tiposala.edit', 'uses'=> 'TipoSalaController@edit']);
    Route::patch('/{tiposala}', ['as'=> 'tiposala.update', 'uses'=> 'TipoSalaController@update']);

    Route::get('/{tiposala}/delete', ['as'=> 'tiposala.destroy', 'uses'=> 'TipoSalaController@destroy']);
});

// Sala
Route::group(['prefix'=> 'salas', 'middleware'=> 'auth'], function(){

    Route::get('/', ['as'=> 'sala.index', 'uses'=> 'SalaController@index'])->where('format', 'json|html');
    Route::get('/{sala}', ['as'=> 'sala.show', 'uses'=> 'SalaController@show'])->where('sala', '[0-9]+');

    Route::get('/create', ['as'=> 'sala.create', 'uses'=> 'SalaController@create']);
    Route::post('/', ['as'=> 'sala.store', 'uses'=> 'SalaController@store']);

    Route::get('/{sala}/edit', ['as'=> 'sala.edit', 'uses'=> 'SalaController@edit']);
    Route::patch('/{sala}', ['as'=> 'sala.update', 'uses'=> 'SalaController@update']);

    Route::get('/{sala}/delete', ['as'=> 'sala.destroy', 'uses'=> 'SalaController@destroy']);
});

Route::auth();

Route::get('/', 'HomeController@index');
