<?php

use Bican\Roles\Models\Role;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(Role::class, function(Faker\Generator $faker){

    $roleName= $faker->unique()->numerify('Rol ##');
    return [
        'name'=> $roleName,
        'slug'=> str_slug($roleName)
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Region::class, function(\Faker\Generator $faker){
   return [
       'nombre'=> $faker->numerify('Región ##')
   ];
});

$factory->define(\App\Edificio::class, function(\Faker\Generator $faker){
    return [
        'nombre'=> $faker->numerify('Edificio ##'),
        'region_id'=> factory(App\Region::class)->create()->id,
        'direccion'=> $faker->streetAddress,
        'cant_pisos'=> $faker->numberBetween(0, 10),
        'cafeteria'=> $faker->boolean
    ];
});

$factory->define(\App\TipoSala::class, function(\Faker\Generator $faker){
    return [
        'nombre'=> $faker->numerify('TipoSala ##')
    ];
});

$factory->define(\App\Sala::class, function(\Faker\Generator $faker){
   return [
       'nombre'=> $faker->numerify('Sala ##'),
       'edificio_id'=> factory(\App\Edificio::class)->create()->id,
       'tiposala_id'=> factory(\App\TipoSala::class)->create()->id,
       'responsable_id'=> factory(\App\User::class)->create()->id,
       'piso'=> $faker->numerify('##'),
       'capacidad_min'=> $faker->numberBetween(1,5),
       'capacidad_max'=> $faker->numberBetween(10,20),
       'hora_ini_activa'=> $faker->time('H:i'),
       'hora_fin_activa'=> $faker->time('H:i'),
       'notas'=> $faker->realText(75)
   ];
});

$factory->define(\App\Attribute::class, function(\Faker\Generator $faker){
    return [
        'name'=> $faker->numerify('color ##'),
        'value'=> $faker->safeColorName
    ];
});

