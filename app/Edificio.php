<?php

namespace Salas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Edificio extends Model
{
    use SoftDeletes;
    protected $table= 'edificios';
    protected $fillable= ['nombre', 'direccion', 'cant_pisos', 'cafeteria'];

    public function region(){

        return $this->belongsTo('App\Region');
    }

    public function salas(){

        return $this->hasMany(Sala::class);
    }
}
