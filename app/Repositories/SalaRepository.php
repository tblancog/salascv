<?php

namespace App\Repositories;

use App\Attribute;
use App\Edificio;
use App\Sala;
use App\TipoSala;
use App\Http\Requests;
use App\Http\Requests\SalaRequest;
use App\User;

Class SalaRepository extends Repository
{
    protected static $sala;

    /**
     * New Sala bussiness logic
     * @param $input
     * @return Sala
     */
    public static function createSala($input){

        static::$sala = Sala::create($input->all());
        static::$sala->attrs()->sync($input->attrs);
        return self::$sala;
    }

    /**
     * Sala update bussiness logic
     * @param Sala $sala
     * @param $input
     * @return Sala
     */
    public static function updateSala(Sala $sala, SalaRequest $input){

        static::$sala= $sala;
        static::$sala->update($input->all());
        static::$sala->save();
        static::$sala->attrs()->sync($input->attrs);
        return static::$sala;
    }
}