<?php

namespace App\Repositories;

use App\User;
use App\Http\Requests;
use App\Http\Requests\UserRequest;

Class UserRepository extends Repository
{
    protected static $user;

    /**
     * New User bussiness logic
     * @param $input
     * @return User
     */
    public static function createUser($input){

        static::$user= User::create($input->all());
        static::$user->save();
        static::detachAndAttachRoles($input->roles);
        return static::$user;
    }

    /**
     * User update bussiness logic
     * @param User $user
     * @param $input
     * @return User
     */
    public static function updateUser(User $user, UserRequest $input){

        static::$user= $user;
        static::$user->update($input->all());
        static::detachAndAttachRoles($input->roles);
        return static::$user;
    }

    /**
     * Detaches and attaches all roles again
     * @param $roles
     */
    public static function detachAndAttachRoles($roles){

        static::$user->detachAllRoles();
        static::$user->attachRole($roles);
    }
}