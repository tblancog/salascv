<?php

namespace App\Repositories;
use Validator;


Abstract Class Repository
{

    public function __call($name, $args)
    {

        return parent::$name($args);
    }

}