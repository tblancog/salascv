<?php

namespace App\Repositories;

use App\TipoSala;
use App\Http\Requests;
use App\Http\Requests\TipoSalaRequest;

Class TipoSalaRepository extends Repository
{
    protected static $tipoSala;

    /**
     * New TipoSala bussiness logic
     * @param $input
     * @return TipoSala
     */
    public static function createTipoSala($input){

        static::$tipoSala= TipoSala::create($input->all());
        static::$tipoSala->save();
        return static::$tipoSala;
    }

    /**
     * TipoSala update bussiness logic
     * @param TipoSala $tipoSala
     * @param $input
     * @return TipoSala
     */
    public static function updateTipoSala(TipoSala $tipoSala, TipoSalaRequest $input){

        static::$tipoSala= $tipoSala;
        static::$tipoSala->update($input->all());
        return static::$tipoSala;
    }
}