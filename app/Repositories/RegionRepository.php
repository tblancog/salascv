<?php

namespace App\Repositories;

use App\Region;
use App\Http\Requests;
use App\Http\Requests\RegionRequest;

Class RegionRepository extends Repository
{
    protected static $region;

    /**
     * New Region bussiness logic
     * @param $input
     * @return Region
     */
    public static function createRegion($input){

        static::$region= Region::create($input->all());
        static::$region->save();
        return static::$region;
    }

    /**
     * Region update bussiness logic
     * @param Region $region
     * @param $input
     * @return Region
     */
    public static function updateRegion(Region $region, RegionRequest $input){

        static::$region= $region;
        static::$region->update($input->all());
        return static::$region;
    }
}