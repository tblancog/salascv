<?php

namespace App\Repositories;

use App\Edificio;
use App\Region;
use App\Http\Requests;
use App\Http\Requests\EdificioRequest;

Class EdificioRepository extends Repository
{
    protected static $edificio;

    /**
     * New Edificio bussiness logic
     * @param $input
     * @return Edificio
     */
    public static function createEdificio($input){
        static::$edificio= new Edificio($input->all());
        static::$edificio->region()->associate(Region::find($input->region));
        static::$edificio->save();
        return static::$edificio;
    }

    /**
     * Edificio update bussiness logic
     * @param Edificio $edificio
     * @param $input
     * @return Edificio
     */
    public static function updateEdificio(Edificio $edificio, EdificioRequest $input){

        static::$edificio= $edificio;
        static::$edificio->update($input->all());
        static::$edificio->region()->associate($input->region);
        return static::$edificio;
    }

    /**
     * Associates Region Entity
     * @param $region
     */
    public static function associateRegion($region){

        static::$edificio->region()->associate($region);
    }
}