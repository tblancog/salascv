<?php

namespace Salas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Salas;

class TipoSala extends Model
{
    use SoftDeletes;
    protected $table= 'tiposala';
    protected $fillable= ['nombre'];

//    public function salas(){
//
//        return $this->hasMany(Sala::class);
//    }
}
