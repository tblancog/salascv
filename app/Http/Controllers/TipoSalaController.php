<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\TipoSalaRequest;
use App\TipoSala;
use App\Repositories\TipoSalaRepository;

class TipoSalaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tiposala.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiposala.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TipoSalaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoSalaRequest $request)
    {
        $tiposala= TipoSalaRepository::createTipoSala($request);
        flash()->success('Tipo de Sala creado', $request->name);
        return redirect()->route('tiposala.show', compact('tiposala'));
    }

    /**
     * Controls update tiposala action
     * @param TipoSalaRequest $request
     * @param TipoSala $tiposala
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TipoSalaRequest $request, TipoSala $tiposala)
    {
        $tiposala= TipoSalaRepository::updateTipoSala($tiposala, $request);
        flash()->success('Tipo de Sala guardado', $request->name);
        return redirect()->route('tiposala.show', compact('tiposala'));
    }

    /**
     * Display the specified resource.
     * @param TipoSala $tiposala
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(TipoSala $tiposala)
    {
        return view('tiposala.show', compact('tiposala'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param TipoSala $tiposala
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(TipoSala $tiposala)
    {
        return view('tiposala.edit', compact('tiposala'));
    }

    /**
     * Remove the specified resource from storage.
     * @param TipoSala $tiposala
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(TipoSala $tiposala)
    {
        $tiposala->delete();
        flash()->success('Tipo de Sala eliminado', $tiposala->name);
        return redirect()->route('tiposala.index');
    }
}
