<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\RegionRequest;
use App\Region;
use App\Repositories\RegionRepository;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('region.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('region.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RegionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegionRequest $request)
    {
        $region= RegionRepository::createRegion($request);
        flash()->success('Región creada', $request->name);
        return redirect()->route('region.show', compact('region'));
    }

    /**
     * Controls update region action
     * @param RegionRequest $request
     * @param Region $region
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RegionRequest $request, Region $region)
    {
        $region= RegionRepository::updateRegion($region, $request);
        flash()->success('Región guardada', $request->name);
        return redirect()->route('region.show', compact('region'));
    }

    /**
     * Display the specified resource.
     * @param Region $region
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Region $region)
    {
        return view('region.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Region $region
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Region $region)
    {
        return view('region.edit', compact('region'));
    }

    /**
     * Remove the specified resource from storage.
     * @param Region $region
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Region $region)
    {
        $region->delete();
        flash()->success('Región eliminada', $region->name);
        return redirect()->route('region.index');
    }
}
