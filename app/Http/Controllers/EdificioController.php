<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\EdificioRequest;
use App\Edificio;
use App\Repositories\EdificioRepository;

class EdificioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('edificio.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('edificio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EdificioRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EdificioRequest $request)
    {
        $edificio= EdificioRepository::createEdificio($request);
        flash()->success('Edificio creado', $request->nombre);
        return redirect()->route('edificio.show', compact('edificio'));
    }

    /**
     * Controls update edificio action
     * @param EdificioRequest $request
     * @param Edificio $edificio
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EdificioRequest $request, Edificio $edificio)
    {
        $edificio= EdificioRepository::updateEdificio($edificio, $request);
        flash()->success('Edificio guardado', $request->nombre);
        return redirect()->route('edificio.show', compact('edificio'));
    }

    /**
     * Display the specified resource.
     * @param Edificio $edificio
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Edificio $edificio)
    {
        return view('edificio.show', compact('edificio'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Edificio $edificio
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Edificio $edificio)
    {
        return view('edificio.edit', compact('edificio'));
    }

    /**
     * Remove the specified resource from storage.
     * @param Edificio $edificio
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Edificio $edificio)
    {
        $edificio->delete();
        flash()->success('Edificio eliminado', $edificio->nombre);
        return redirect()->route('edificio.index');
    }
}
