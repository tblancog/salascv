<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Http\Requests;
use App\Http\Requests\SalaRequest;
use App\Sala;
use App\Repositories\SalaRepository;

class SalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sala.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sala.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SalaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalaRequest $request)
    {
        $sala= SalaRepository::createSala($request);
        flash()->success('Sala creada', $request->nombre);
        return redirect()->route('sala.show', compact('sala'));
    }

    /**
     * Controls update sala action
     * @param SalaRequest $request
     * @param Sala $sala
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SalaRequest $request, Sala $sala)
    {
        $sala= SalaRepository::updateSala($sala, $request);
        flash()->success('Sala guardada', $request->nombre);
        return redirect()->route('sala.show', compact('sala'));
    }

    /**
     * Display the specified resource.
     * @param Sala $sala
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Sala $sala)
    {
        return view('sala.show', compact('sala'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Sala $sala
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Sala $sala)
    {
        return view('sala.edit', compact('sala'));
    }

    /**
     * Remove the specified resource from storage.
     * @param Sala $sala
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Sala $sala)
    {
        $sala->delete();
        flash()->success('Sala eliminada', $sala->nombre);
        return redirect()->route('sala.index');
    }
}
