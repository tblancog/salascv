<?php

namespace App\Http\Requests;

class SalaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sala= $this->route()->getParameter('sala');
        $id= ($sala) ? $sala->id : null;
        return [
            'nombre'=> 'required|max:50|unique:salas,nombre,'.$id,
            'edificio_id'=> 'required',
            'piso'=> 'required',
            'tiposala_id'=> 'required',
            'responsable_id'=> 'required',
            'capacidad_min'=> 'required|integer|min:0|max:50',
            'capacidad_max'=> 'required|integer|min:0|max:150',
            'hora_ini_activa'=> 'date_format:H:i',
            'hora_fin_activa'=> 'date_format:H:i',
            'notas'=> 'max:150'
        ];
    }
}
