<?php

namespace App\Http\Requests;

class TipoSalaRequest extends Request
{
    /**
     * Determine if the tipoSala is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $tipoSala= $this->route()->getParameter('tiposala');
        $id= ($tipoSala) ? $tipoSala->id : null;
        return [
            'nombre'=> 'required|unique:tiposala|max:50,nombre,'.$id
        ];
    }
}
