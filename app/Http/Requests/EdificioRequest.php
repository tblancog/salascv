<?php

namespace App\Http\Requests;

class EdificioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $edificio= $this->route()->getParameter('edificio');
        $id= ($edificio) ? $edificio->id : null;
        return [
            'nombre'=> 'required|max:50|unique:edificios,nombre,'.$id,
            'direccion'=> 'required',
            'region'=> 'required',
            'cant_pisos'=> 'required',
            'cafeteria'=>  'required'
        ];
    }
}
