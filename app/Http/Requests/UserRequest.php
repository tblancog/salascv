<?php

namespace App\Http\Requests;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user= $this->route()->getParameter('user');
        $id= ($user) ? $user->id : null;
        return [
            'name' => 'required',
            'username' => "required|max:50|unique:users,username,".$id,
            'roles'=> 'required'
        ];
    }
}
