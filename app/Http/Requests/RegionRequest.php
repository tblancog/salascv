<?php

namespace App\Http\Requests;

class RegionRequest extends Request
{
    /**
     * Determine if the region is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $region= $this->route()->getParameter('region');
        $id= ($region) ? $region->id : null;
        return [
            'nombre'=> 'required|unique:regiones|max:50,nombre,'.$id
        ];
    }
}
