<?php

// Hello
namespace Salas;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table= 'attribute';

    /**
     * Get attributes from a sala relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function salas(){

        return $this->belongsToMany(Sala::class, 'attribute_salas', 'attribute_id', 'salas_id');
    }
}
