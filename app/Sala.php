<?php

namespace Salas;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sala extends Model
{
    use SoftDeletes;
    protected $table= 'salas';
    protected $fillable= ['nombre',
        'piso',
        'uso_exclusivos',
        'edificio_id',
        'tiposala_id',
        'responsable_id',
        'capacidad_min',
        'capacidad_max',
        'hora_ini_activa',
        'hora_fin_activa',
        'notas'
    ];

    /**
     * Gets edificio relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function edificio(){

        return $this->belongsTo(Edificio::class);
    }

    /**
     * Gets tiposala relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tiposala(){

        return $this->belongsTo(TipoSala::class);
    }


    /**
     * Gets user relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function responsable(){

        return $this->belongsTo(User::class);
    }

    /**
     * Gets attributes relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attrs(){

        return $this->belongsToMany(Attribute::class, 'attribute_salas', 'salas_id', 'attribute_id');
    }
}
