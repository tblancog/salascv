<?php

namespace Salas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use SoftDeletes;

    protected $table= 'regiones';
    protected $fillable= ['nombre'];

    public function edificios(){

        return $this->hasMany('App\Edificio');
    }
}
