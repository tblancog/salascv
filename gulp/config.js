var out = './build/';
var publicdir = out + '/public/';

var dirs = {
    "src":        './',
    "out":        out,
    "publicdir":  publicdir,
    "bower":      './bower_components/',
    "npm":        './node_modules/'
};

var env= {
    "name": 'deploy',
    "envfile": { "local": dirs.src + 'local.env',
                 "dev":   dirs.src + 'dev.env',
                 "test":  dirs.src + 'test.env',
                 "prod":  dirs.src + 'prod.env'},
    "outputEnvFile": '.env'
};

var ssh = {
    "dev": {
        "username": 'admindesa',
        "host": 'srv-webcvdesa',
        "dest": '/var/www/html'
    },
    "prod": {
        "username": 'tblanco',
        "host": 'localhost',
        "dest": '/var/www/'
    }
};

var assets = {
    "resources":  dirs.src + 'resources/assets/',
    "fonts": {
        "src": [dirs.npm + 'font-awesome/fonts/**/*',
                dirs.npm + 'bootstrap-sass/assets/fonts/**/*'],
        "out": dirs.publicdir + 'fonts'
    },
    "styles": {
        sassOptions: {
            "style": { outputStyle: 'compressed' }
        },
        "out": dirs.publicdir + 'styles'
    },
    "scripts":{
        "src": [
            dirs.npm + 'jquery/dist/jquery.js',
            dirs.npm + 'bootstrap-sass/assets/javascripts/bootstrap.js',
            dirs.npm + 'moment/moment.js',
            dirs.npm + 'moment/locale/es.js',
            dirs.npm + 'eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
            dirs.npm + 'select2/src/js/jquery.select2.js',
            dirs.npm + 'sweetalert/lib/sweetalert.js'
        ],
        "user": [
            dirs.src + 'resources/assets/js/app.js'
        ],
        "out": dirs.publicdir + 'js'
    }
};

var build = {
    "src": [ dirs.out + '**/*',
        '../' + dirs.out + 'docs/'
    ]
};

var ignore = [
    '!'+ dirs.src + '{tests,tests/**}',
    '!'+ dirs.src + '{node_modules,node_modules/**}',
    '!'+ dirs.src + '{gulp,gulp/**}',
    '!'+ dirs.src + '{build,build/**}',
    '!'+ dirs.src + 'bootstrap/{cache,cache/**}',
    '!'+ dirs.src + 'storage/framework/{cache,cache/**}',
    '!'+ dirs.src + 'storage/{logs,logs/**}',
    '!'+ dirs.src + 'resources/{assets,assets/**}',
    '!'+ dirs.src + '{logs,logs/**}',
    '!'+ dirs.src + 'composer.*',
    '!'+ dirs.src + 'bower.json',
    '!'+ dirs.src + 'gulpfile.js',
    '!'+ dirs.src + 'package.json',
    '!'+ dirs.src + 'phpunit.xml',
    '!'+ dirs.src + 'readme.md',
    '!'+ dirs.src + '{local,dev,test,prod}.env'
];


var watch = {
    "styles": {
        "src": [ assets.resources + 'css/**/*' ],
        "tasks": ['styles']
    },
    "scripts": {
        "src": [ assets.resources + 'js/**/*' ],
        "tasks": ['user-scripts']
    },
    "backend": {
        "src": [ dirs.src + '**/*.php' ],
        "ignore": [ '!'+ dirs.src + 'vendor',
                    '!'+ dirs.out
        ],
        "tasks": ['build']
    }

};



module.exports = { "dirs": dirs, "env": env, "assets": assets, "ssh": ssh, "build": build, "ignore": ignore, "watch": watch };