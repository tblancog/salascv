var gulp= require('gulp'),
    uglify= require('gulp-uglify'),
    autoprefixer= require('gulp-autoprefixer'),
    notify= require('gulp-notify'),
    sass= require('gulp-sass'),
    concat= require('gulp-concat'),
    rename= require('gulp-rename'),
    clean= require('gulp-clean'),
    merge= require('merge-stream'),
    changed= require('gulp-changed'),
    sourcemaps= require('gulp-sourcemaps'),
    // Minimist - https://www.npmjs.com/package/minimist
    argv  = require('minimist')(process.argv),
    // https://www.npmjs.com/package/gulp-util
    gutil = require('gulp-util'),
    config= require('../config.js')
    ;

gulp.task('fonts', function(){
    console.log(config.assets.fonts.out);
    gulp.src( config.assets.fonts.src )
        .pipe(gulp.dest( config.assets.fonts.out ));
});

gulp.task('styles', function(){
    return gulp.src( config.assets.resources + 'css/**' )
        .pipe( changed( config.dirs.publicdir + 'css/' ))
        .pipe(sourcemaps.init())
        .pipe(sass( argv.prod && config.assets.sassOptions.style )
              .on('error', sass.logError))
        .pipe(sourcemaps.write('maps'))
        .pipe( gulp.dest( config.dirs.publicdir + 'css/') );
});

/**
 * Will generate a single scripts file, also it will
 * uglify only if "prod" option is activated
 * e.g. gulp scripts --prod
 * */
gulp.task('scripts', function(){

    return gulp.src( config.assets.scripts.src )
        .pipe(argv.prod ? uglify() : gutil.noop())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest( config.assets.scripts.out ))
});

gulp.task('user-scripts', function(){{
    gulp.src( config.assets.scripts.user )
        .pipe( changed( config.assets.scripts.out ) )
        .pipe(argv.prod ? uglify() : gutil.noop())
        .pipe(concat('myapp.min.js'))
        .pipe(gulp.dest(config.assets.scripts.out));
}});

gulp.task('build',  function(){

    var files =  [ config.dirs.src + '**/*' ];
    gulp.src( files.concat( config.ignore) )
        .pipe(changed( config.dirs.out ))
        .pipe(gulp.dest( config.dirs.out ));
});

gulp.task('env', function(){

    var envfile = (argv.local && config.env.envfile.local) ||
                  (argv.dev   && config.env.envfile.dev)   ||
                  (argv.test  && config.env.envfile.test)  ||
                  (argv.prod  && config.env.envfile.prod);

    return gulp.src( envfile )
            .pipe(rename( config.env.outputEnvFile ))
            .pipe(gulp.dest( config.dirs.out ))
});

gulp.task('clean', function(){
    return gulp.src(config.dirs.out, { read: false})
        .pipe(clean());
});

gulp.task('default', ['build', 'fonts', 'styles', 'scripts', 'user-scripts', 'env']);

gulp.task('watch', ['default'], function(){
    var watchConf= config.watch;

    // Styles
    gulp.watch( watchConf.styles.src, watchConf.styles.tasks);

    //  Scripts
    gulp.watch( watchConf.scripts.src, watchConf.scripts.tasks);

    // Backend
    var backendFiles = watchConf.backend.src;
    backendFiles = backendFiles.concat( watchConf.backend.ignore );
    gulp.watch( backendFiles, watchConf.backend.tasks );

});

//picOfTheDay

