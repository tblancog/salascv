@extends('layouts.app')
@section('content')
    @include('forms.parent-form', ['title'=> 'Editar Usuario',
                                   'model'=> $user,
                                   'selected_roles'=> $user->roles->pluck('id')->toArray(),
                                   'route'=> 'user.update',
                                   'method'=> 'PATCH',
                                   'view'=> 'forms.user',
            ])

@stop