@extends('layouts.app')
@inject('users', 'App\User')

<div class="container">
    <div class="row">
        <h2>Usuarios</h2>
        <a class="btn btn-primary btn-sm" href="{{ route('user.create') }}">
            <i class="fa fa-plus">&nbsp;</i> Añadir Usuario
        </a>
    </div>
    <div class="row">

        @if(count($users::all()))
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users::all() as $user)
                        <tr>
                        <td>{{ link_to('users/'.$user->id, $user->name) }}</td>
                        <td>{{ $user->username }}</td>
                        <td><a class="btn btn-sm btn-default" href="{{ route('user.edit', ['id'=> $user->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>
                        <a class="btn btn-sm btn-danger" href="{{ route('user.destroy', ['id'=> $user->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h4 class="text-center">No existen registros</h4>
        @endif
    </div>
</div>



{{--@section('content')--}}
    {{--@include('forms.list', ['title'=> 'Usuarios',--}}
                           {{--'routes'=> ['create'=> route('user.create'), 'title'=> 'Crear Usuario'],--}}
                           {{--'fields'=> ['Nombre', 'Usuario', '&nbsp;'],--}}
                           {{--'entities'=> $users]);--}}
    {{--@section('entities-list')--}}
        {{--<tbody>--}}
        {{--@foreach($users::all() as $user)--}}
            {{--<tr>--}}
                {{--<td>{{ link_to('users/'.$user->id, $user->name) }}</td>--}}
                {{--<td>{{ $user->username }}</td>--}}
                {{--<td><a class="btn btn-sm btn-default" href="{{ route('user.edit', ['id'=> $user->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>--}}
                {{--<a class="btn btn-sm btn-danger" href="{{ route('user.destroy', ['id'=> $user->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>--}}
            {{--</tr>--}}
        {{--@endforeach--}}
        {{--</tbody>--}}
    {{--@stop--}}
{{--@stop--}}

{{--@section('footer')--}}
{{--@stop--}}