@extends('layouts.app')

@section('content')
    @inject('roles', 'Bican\Roles\Models\Role')
    @include('forms.parent-form', ['title'=> 'Añadir Usuario',
                                   'model'=> new \App\User(),
                                   'selected_roles'=> [],
                                   'route'=> 'user.store',
                                   'method'=> 'POST',
                                   'view'=> 'forms.user',
                                   ]);
@stop