@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2>{{ $user->name }}</h2>
        </div>
        <div class="row">
            <h5>{{ $user->username }}</h5>
        </div>
        <div class="row">
            <h5>{{ $user->roles->pluck('name')->implode(', ') }}</h5>
        </div>
    </div>
@endsection