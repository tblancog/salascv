@extends('layouts.app')

@section('content')
    @include('forms.parent-form', ['title'=> 'Añadir Región',
                                   'model'=> new \App\Region(),
                                   'route'=> 'region.store',
                                   'method'=> 'POST',
                                   'view'=> 'forms.region',])

@stop