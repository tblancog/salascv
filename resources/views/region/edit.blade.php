@extends('layouts.app')
@section('content')
    @include('forms.parent-form', ['title'=> 'Editar Región',
                                   'model'=> $region,
                                   'route'=> 'region.update',
                                   'method'=> 'PATCH',
                                   'view'=> 'forms.region',
            ])

@stop