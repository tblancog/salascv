@extends('layouts.app')
@inject('regions', 'App\Region')

<div class="container">
    <div class="row">
        <h2>Regiones</h2>
        <a class="btn btn-primary btn-sm" href="{{ route('region.create') }}">
            <i class="fa fa-plus">&nbsp;</i> Añadir Región
        </a>
    </div>
    <div class="row">

        @if(count($regions::all()))
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($regions::all() as $region)
                        <tr>
                            <td>{{ link_to('regions/'.$region->id, $region->nombre) }}</td>
                            <td><a class="btn btn-sm btn-default" href="{{ route('region.edit', ['id'=> $region->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>
                                <a class="btn btn-sm btn-danger" href="{{ route('region.destroy', ['id'=> $region->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h4 class="text-center">No existen registros</h4>
        @endif
    </div>
</div>
