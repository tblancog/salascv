{{-- View data injections --}}
@inject('edificios', 'App\Edificio')
@inject('tiposalas', 'App\TipoSala')
@inject('responsables', 'App\User')
@inject('attrs', 'App\Attribute')

@extends('layouts.app')
@section('content')

    @include('forms.parent-form', ['title'=> 'Editar Sala',
                                   'model'=> $sala,
                                   'route'=> 'sala.update',
                                   'data'=> [
                                        'edificios' => $edificios->all()->lists('nombre', 'id')->toArray(),
                                        'tiposalas' => $tiposalas->all()->lists('nombre', 'id')->toArray(),
                                        'responsables' => $responsables->all()->lists('name', 'id')->toArray(),
                                        'attributes' => $attrs->lists('name','id')->toArray()
                                   ],
                                   'selected'=> [
                                        'attributes' => $sala->attrs->lists('id')->toArray()
                                   ],
                                   'method'=> 'PATCH',
                                   'view'=> 'forms.sala',
            ])

@stop