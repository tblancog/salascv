@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h2>{{ $sala->nombre }}</h2>
            <h4>{{ $sala->edificio->nombre }}</h4>
            <h4>{{ $sala->piso }}</h4>
            <h4>{{ $sala->tiposala->nombre }}</h4>
            <h4>{{ $sala->responsable->name }} </h4>
            <h4>{{ $sala->capacidad_min }}</h4>
            <h4>{{ $sala->capacidad_max }}</h4>
            <h4>{{ $sala->hora_ini_activa }}</h4>
            <h4>{{ $sala->hora_fin_activa }}</h4>
            <h4>{{ $sala->attrs->lists('name', 'value') }}</h4>
            <h4>{{ $sala->notas }}</h4>
        </div>
    </div>
@endsection