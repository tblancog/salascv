@extends('layouts.app')
@inject('salas', 'App\Sala')

<div class="container">
    <div class="row">   
        <h2>Salas</h2>
        <a class="btn btn-primary btn-sm" href="{{ route('sala.create') }}">
            <i class="fa fa-plus">&nbsp;</i> Añadir Sala
        </a>
    </div>
    <div class="row">

        @if(count($salas::all()))
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($salas::all() as $sala)
                        <tr>
                            <td>{{ link_to('salas/'.$sala->id, $sala->nombre) }}</td>
                            <td><a class="btn btn-sm btn-default" href="{{ route('sala.edit', ['id'=> $sala->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>
                                <a class="btn btn-sm btn-danger" href="{{ route('sala.destroy', ['id'=> $sala->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h4 class="text-center">No existen registros</h4>
        @endif
    </div>
</div>
