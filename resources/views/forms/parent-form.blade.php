<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>{{ $title }}</h2>
            @include('partials.flash_msg')

            @include($view)
        </div>
    </div>
</div>