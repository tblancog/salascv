{!! Form::model($model, ['route'=> [$route, $model->id],
                         'method'=> $method,
                         'novalidate'=> 'novalidate']) !!}
<div class="row">
    @include('forms.fields.edificio')
</div>
<div class="row">
    <div class="form-group">
        {{ Form::submit('Guardar', ['class'=> 'btn btn-primary'] ) }}
        {{ link_to_route('edificio.index', 'Atrás', [], ['class'=> 'btn btn-default']) }}
    </div>
</div>
{!! Form::close() !!}