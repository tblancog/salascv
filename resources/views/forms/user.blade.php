{!! Form::model($model, ['route'=> [$route, $model->id],
                         'method'=> $method,
                         'novalidate'=> 'novalidate']) !!}
<div class="row">
    @include('forms.fields.user')
</div>

{!! Form::close() !!}