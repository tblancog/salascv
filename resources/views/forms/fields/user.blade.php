{{-- View data injections --}}
@inject('roles', 'Bican\Roles\Models\Role')


<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::text('name', null, ['placeholder'=> 'Nombre', 'class'=> 'form-control']) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::text('username', null, ['placeholder'=> 'Usuario', 'class'=> 'form-control']) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{  Form::select('roles[]', $roles->all()->lists('name', 'id'),
                                            $selected_roles,
                                 ['id'=> 'select2-roles', 'class'=> 'form-control select2', 'multiple'])  }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::submit('Guardar', ['class'=> 'btn btn-primary'] ) }}
                {{ link_to_route('user.index', 'Atrás', [], ['class'=> 'btn btn-default']) }}
            </div>
        </div>
    </div>

</div>

{{-- Script section --}}
@section('footer-scripts')

    <script src="{{ asset('js/vendor/select2.min.js') }}"></script>
    <script>
       $("#select2-roles").select2(
               {
                   allowClear: true,
                   placeholder: 'Seleccione Roles'
               }
       );
    </script>
@endsection
