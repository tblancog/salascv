{{-- First column --}}
<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('nombre', 'Nombre o alias de Sala') }}
        {{ Form::text('nombre', NULL, ['placeholder'=> 'Nombre o número', 'class'=> 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('edificio_id', 'Edificio') }}
        {{  Form::select('edificio_id', [NULL => '(Vacio)'] + $data['edificios'],
                                         NULL,
                                         ['id'=> 'select2-edificio', 'class'=> 'form-control select2'], NULL)  }}
    </div>
    <div class="form-group">
        {{ Form::label('piso', 'Piso') }}
        {{ Form::text('piso', NULL, ['placeholder'=> 'Nombre o número', 'class'=> 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('tiposala_id', 'Tipo de Sala') }}
        {{  Form::select('tiposala_id', [NULL => '(Vacio)'] + $data['tiposalas'],
                                         NULL,
                                         ['id'=> 'select2-tiposala', 'class'=> 'form-control select2'], NULL)  }}
    </div>
</div>

{{-- Second column --}}
<div class="col-md-6">
    <div class="form-group">
        {{ Form::label('responsable_id', 'Responsable de Sala') }}
        {{ Form::select('responsable_id', [NULL => '(Vacio)'] + $data['responsables'],
                                            NULL,
                                            ['id'=> 'select2-responsable', 'class'=> 'form-control select2'], NULL)  }}
    </div>

    {{-- Capacidad de Sala --}}
    <div class="row">
        <div class="col-md-12">
            {{ Form::label(NULL, 'Capacidad de Sala') }}
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('capacidad_min', 'Min.') }}
                {{ Form::number('capacidad_min', NULL, ['class'=> 'form-control', 'id'=> 'capacidad_min', 'min'=> '0', 'max'=> '50']) }}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('capacidad_max', 'Max.') }}
                {{ Form::number('capacidad_max', NULL, ['class'=> 'form-control', 'id'=> 'capacidad_max', 'min'=> '0', 'max'=> '150']) }}
            </div>
        </div>
    </div>

    {{--Horario de Sala --}}
    <div class="row">
        <div class="col-md-12">
            {{ Form::label(NULL, 'Horario de Sala') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('hora_ini', 'Desde') }}
                <div class='input-group datetimepicker' id='hora_ini_activa'>
                    {{ Form::text('hora_ini_activa', '09:00', ['placeholder'=> 'hh:mm', 'class'=> 'form-control', 'id'=> 'hora_ini']) }}
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                 </span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('hora_fin', 'Hasta') }}
                <div class='input-group datetimepicker' id='hora_fin_activa'>
                    {{ Form::text('hora_fin_activa', '18:00', ['placeholder'=> 'hh:mm', 'class'=> 'form-control', 'id'=> 'hora_fin']) }}
                    <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
             </span>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Last column --}}
<div class="col-md-12">
    <div class="form-group">
        {{ Form::label('attrs', 'Atributos de la sala') }}
        {{  Form::select('attrs[]', $data['attributes'],
                          $selected['attributes'],
                         ['id'=> 'select2-attributes', 'class'=> 'form-control select2', 'multiple'=>  'multiple'], NULL)  }}
    </div>
    <div class="form-group">
        {{ Form::label('notas', 'Notas') }}
        {{ Form::textarea('notas', NULL, ['placeholder'=> 'Notas sobre la sala', 'class'=> 'form-control', 'rows'=> 2]) }}
    </div>
</div>