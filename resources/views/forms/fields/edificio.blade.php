@inject('regions', 'App\Region')
<div class="form-group">
    {{ Form::text('nombre', null, ['placeholder'=> 'Nombre', 'class'=> 'form-control']) }}
</div>
<div class="form-group">
    {{  Form::select('region', $regions->all()->lists('nombre', 'id'),
                               $selected_region,
                     ['id'=> 'select2-region', 'class'=> 'form-control select2'], null)  }}
</div>
<div class="form-group">
    {{ Form::text('direccion', null, ['placeholder'=> 'Dirección', 'class'=> 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::number('cant_pisos', null, ['placeholder'=> 'Cantidad de pisos', 'class'=> 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::checkbox('cafeteria', 1) }} {{ Form::label('cafeteria', 'Servicio de cafetería') }}
</div>
@section('footer')
    <script>
        $("#select2-region").select2(
                {   allowClear: true,
                    placeholder: 'Seleccione Región' }
        );
    </script>
@stop