<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistema de Reserva de Salas</title>

    {{-- Head css area--}}
    <link rel="stylesheet" href=" {{ asset('css/app.css') }}">
    {{--@yield('css')--}}

    <script type="text/javascript" src="{{ asset('js/scripts.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/myapp.min.js') }}"></script>

</head>
<body id="app-layout">
    @include('partials.navbar')

    @yield('content')

    @include('flash')

    @yield('footer')

</body>
</html>
