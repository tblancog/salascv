@extends('layouts.app')
@inject('tiposala', 'App\TipoSala')

<div class="container">
    <div class="row">
        <h2>TipoSala</h2>
        <a class="btn btn-primary btn-sm" href="{{ route('tiposala.create') }}">
            <i class="fa fa-plus">&nbsp;</i> Añadir Tipo de Sala
        </a>
    </div>
    <div class="row">

        @if(count($tiposala::all()))
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tiposala::all() as $tiposala)
                        <tr>
                            <td>{{ link_to('tiposala/'.$tiposala->id, $tiposala->nombre) }}</td>
                            <td><a class="btn btn-sm btn-default" href="{{ route('tiposala.edit', ['id'=> $tiposala->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>
                                <a class="btn btn-sm btn-danger" href="{{ route('tiposala.destroy', ['id'=> $tiposala->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h4 class="text-center">No existen registros</h4>
        @endif
    </div>
</div>
