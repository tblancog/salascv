@extends('layouts.app')

@section('content')
    @include('forms.parent-form', ['title'=> 'Añadir Tipo de Sala',
                                   'model'=> new \App\TipoSala(),
                                   'route'=> 'tiposala.store',
                                   'method'=> 'POST',
                                   'view'=> 'forms.tiposala',])

@stop