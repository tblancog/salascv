@extends('layouts.app')
@section('content')
    @include('forms.parent-form', ['title'=> 'Editar Tipo de Sala',
                                   'model'=> $tiposala,
                                   'route'=> 'tiposala.update',
                                   'method'=> 'PATCH',
                                   'view'=> 'forms.tiposala',
            ])

@stop