@extends('layouts.app')

@section('content')
    @include('forms.parent-form', ['title'=> 'Añadir Edificio',
                                   'model'=> new \App\Edificio(),
                                   'selected_region'=> [],
                                   'route'=> 'edificio.store',
                                   'method'=> 'POST',
                                   'view'=> 'forms.edificio',])

@stop