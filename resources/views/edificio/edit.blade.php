@extends('layouts.app')

@section('content')
{{--    {{ dd($edificio->region) }}--}}
    @include('forms.parent-form', ['title'=> 'Editar Edificio',
                                   'model'=> $edificio,
                                   'selected_region'=> $edificio->region->id,
                                   'route'=> 'edificio.update',
                                   'method'=> 'PATCH',
                                   'view'=> 'forms.edificio',
            ])

@stop