@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h2>{{ $edificio->nombre }}</h2>
            <h4>{{ $edificio->region->nombre }}</h4>
            <h4>{{ $edificio->direccion }}</h4>
            <h4>{{ $edificio->cant_pisos }}</h4>
            <h4>{{ $edificio->cafeteria }}</h4>
        </div>
    </div>
@endsection