@extends('layouts.app')
@inject('edificios', 'App\Edificio')

<div class="container">
    <div class="row">   
        <h2>Edificios</h2>
        <a class="btn btn-primary btn-sm" href="{{ route('edificio.create') }}">
            <i class="fa fa-plus">&nbsp;</i> Añadir Edificio
        </a>
    </div>
    <div class="row">

        @if(count($edificios::all()))
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($edificios::all() as $edificio)
                        <tr>
                            <td>{{ link_to('edificios/'.$edificio->id, $edificio->nombre) }}</td>
                            <td><a class="btn btn-sm btn-default" href="{{ route('edificio.edit', ['id'=> $edificio->id]) }}"><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;Editar</a>
                                <a class="btn btn-sm btn-danger" href="{{ route('edificio.destroy', ['id'=> $edificio->id]) }}"><i class="fa fa-trash-o fa-lg"></i>&nbsp;&nbsp;Eliminar</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <h4 class="text-center">No existen registros</h4>
        @endif
    </div>
</div>
