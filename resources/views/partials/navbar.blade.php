<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Sistema de Reservación de Salas</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>{!! link_to_action('HomeController@index',  'Home') !!}</li>
                <li>{!! link_to_action('UserController@index', "Usuarios") !!}</li>
                <li>{!! link_to_action('RegionController@index', "Regiones") !!}</li>
                <li>{!! link_to_action('TipoSalaController@index', "Tipos de Sala") !!}</li>
                <li>{!! link_to_action('SalaController@index', "Salas") !!}</li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
